// Used to get a nice project name in Intellij IDEA
name := Settings.name

lazy val root = project.in(file(".")).
  aggregate(js, jvm).
  settings(
    // Disable publishing of the root project
    publish      := {},
    publishLocal := {}
  )

lazy val cross = crossProject.in(file("."))
  .settings(
    name           := Settings.name,
    normalizedName := Settings.normalizedName,
    version        := Settings.version,
    organization   := Settings.organization,
    scalaVersion   := Settings.versions.scala,

    homepage       := Settings.homepage,
    licenses       += Settings.licenses,
    scmInfo        := Settings.scmInfo,

    publishTo <<= version { Settings.publishTo() },
    publishMavenStyle := true,
    publishArtifact in Test := false,

    pomExtra             := Settings.pomExtra,
    pomIncludeRepository := { _ => false },

    libraryDependencies ++= Settings.sharedDependencies.value
  )
  .settings(Settings.resolverSettings: _*)
  .jvmSettings(
    libraryDependencies ++= Settings.jvmDependencies.value
  )
  .jsSettings(
    libraryDependencies ++= Settings.scalajsDependencies.value,
    jsDependencies      ++= Settings.jsDependencies.value
  )

lazy val jvm = cross.jvm
lazy val js  = cross.js
